﻿namespace DemoFollowersFollowing.Data.Entities
{
    public class DemoUserFollower
    {
        public DemoUser Follower { get; set; }
        public int FollowerId { get; set; }

        public DemoUser Following { get; set; }
        public int FollowingId { get; set; }
    }
}
