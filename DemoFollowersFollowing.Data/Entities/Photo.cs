﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoFollowersFollowing.Data.Entities
{
    public class Photo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public int AlbumId { get; set; }

        public Album Album { get; set; }
    }
}
