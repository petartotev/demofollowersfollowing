﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DemoFollowersFollowing.Data.Entities
{
    public class DemoUser : IdentityUser<int>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<DemoUserFollower> Followers { get; set; }

        public ICollection<DemoUserFollower> Followings { get; set; }
    }
}
