﻿using DemoFollowersFollowing.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DemoFollowersFollowing.Data
{
    public class DemoDbContext : IdentityDbContext<DemoUser, Role, int>
    {
        private readonly IConfiguration _configuration;

        public DemoDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public DbSet<Photo> Photos { get; set; }

        public DbSet<Album> Albums { get; set; }

        public DbSet<DemoUserFollower> Followers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
            builder.Entity<DemoUser>().ToTable("Users", "dbo");

            builder.Entity<DemoUserFollower>(b =>
            {
                b.HasKey(k => new { k.FollowerId, k.FollowingId });

                b.HasOne(o => o.Follower)
                .WithMany(f => f.Followings)
                .HasForeignKey(o => o.FollowerId)
                .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(o => o.Following)
                .WithMany(f => f.Followers)
                .HasForeignKey(o => o.FollowingId)
                .OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}
